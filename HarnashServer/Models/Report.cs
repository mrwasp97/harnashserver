﻿using HarnashServer.Services;

namespace HarnashServer.Models
{
    public class Report
    {
        public string TransportNumber;
        public ReportType ReportType;
        public int Delay;
        public int NumberOfReports;
        public double Lat;
        public double Lng;
        public string From;
        public string To;
        public string Stop;
        public int Hour;
        public int Minute;

        public Report()
        {

        }

        public Report(ReportReceive report, LatLang location)
        {
            TransportNumber = report.Line;
            ReportType = report.ReportType;
            Delay = report.Delay;
            NumberOfReports = 0;
            Lat = location.Lat;
            Lng = location.Lng;
            From = report.From;
            To = report.To;
        }
    }
}