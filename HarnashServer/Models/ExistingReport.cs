﻿using HarnashServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class ExistingReport
    {
        public Report Report;
        public string Stop;
        public int Hour; //Original of the first report
        public int Minute;

        public DateTime ExpiresIn;

        public ExistingReport(ReportReceive reportReceive, LatLang location)
        {
            Report = new Report(reportReceive, location);
            Stop = reportReceive.Stop;
            Hour = reportReceive.Hour;
            Minute = reportReceive.Minute;
            Refresh();
        }

        public bool Match(ReportReceive report, IDataService dataService)
        {
            return Report.From == report.From && Report.To == report.To && Report.TransportNumber == report.Line && dataService.CompareTime(report, this) && report.ReportType == this.Report.ReportType;
        }

        internal void Refresh()
        {
            ExpiresIn = DateTime.Now.AddMinutes(2);
        }
    }
}
