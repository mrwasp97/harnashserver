﻿namespace HarnashServer.Models
{
    public class Stop
    {
        public string Name { get; private set; }
        public string Id { get; private set; }
        public int Delay { get; private set; }
        public double Lat { get; private set; }
        public double Lng { get; private set; }

        public Stop(string name, string id, int delay, double lat, double lng)
        {
            Name = name;
            Id = id;
            Delay = delay;
            Lat = lat;
            Lng = lng;
        }
    }
}