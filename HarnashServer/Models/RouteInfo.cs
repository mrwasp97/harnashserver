﻿using System.Collections.Generic;
using System.Linq;

namespace HarnashServer.Models
{
    public class RouteInfo
    {
        public string From { get; private set; }
        public string To { get; private set; }
        public IEnumerable<ServerStop> Stops => stops.AsEnumerable();
        private List<ServerStop> stops;

        public RouteInfo(string from, string to, List<ServerStop> stops)
        {
            this.stops = new List<ServerStop>();
            foreach(var s in stops)
            {
                this.stops.Add(s);
            }

            From = from;
            To = to;
        }
    }
}