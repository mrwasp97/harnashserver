﻿using System.Collections.Generic;

namespace HarnashServer.Models
{
    public class TimeInfo
    {
        public int Hour;
        public int Minute;

        public TimeInfo(int hour, int minute)
        {
            this.Hour = hour;
            this.Minute = minute;
        }

        public static List<TimeInfo> Mock()
        {
            return new List<TimeInfo>
            {
                new TimeInfo(9, 10),
                new TimeInfo(9, 14),
                new TimeInfo(9, 17),
                new TimeInfo(9, 19),
                new TimeInfo(9, 26),
                new TimeInfo(9, 37),
                new TimeInfo(9, 47),

                new TimeInfo(14, 10),
                new TimeInfo(14, 14),
                new TimeInfo(14, 17),
                new TimeInfo(14, 19),
                new TimeInfo(14, 26),
                new TimeInfo(14, 37),
                new TimeInfo(19, 47),
            };
        }
    }
}
