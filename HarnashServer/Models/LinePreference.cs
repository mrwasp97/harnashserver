﻿using HarnashServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class LinePreference
    {
        public string Line;
        public int Hour;
        public int Minute;
        public string BusStation;

        public override bool Equals(object obj)
        {
            var preference = obj as LinePreference;
            return preference != null &&
                   Line == preference.Line &&
                   Hour == preference.Hour &&
                   Minute == preference.Minute &&
                   BusStation == preference.BusStation;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Line, Hour, Minute, BusStation);
        }

        public static bool operator ==(LinePreference preference1, LinePreference preference2)
        {
            return EqualityComparer<LinePreference>.Default.Equals(preference1, preference2);
        }

        public static bool operator !=(LinePreference preference1, LinePreference preference2)
        {
            return !(preference1 == preference2);
        }

        public bool Match(ExistingReport report, IDataService dataService)
        {
            return this.Line == report.Report.TransportNumber && dataService.CompareTime(this, report);
        }
    }
}
