﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class LineInfo
    {
        private List<RouteInfo> routes;
        public IEnumerable<RouteInfo> Routes => routes.AsEnumerable();

        public LineInfo(List<RouteInfo> routes)
        {
            this.routes = new List<RouteInfo>();
            foreach(var r in routes)
            {
                this.routes.Add(r);
            }
        }
    }
}
