﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class ReportReceive
    {
        public string Line;
        public int Delay;
        public int Hour;
        public int Minute;
        public ReportType ReportType;
        public string Stop;
        public string From;
        public string To;
    }
}
