﻿using System;
using System.Collections.Generic;

namespace HarnashServer.Models
{
    public class LatLang
    {
        public double Lat;
        public double Lng;

        public override bool Equals(object obj)
        {
            var lang = obj as LatLang;
            return lang != null &&
                   Lat == lang.Lat &&
                   Lng == lang.Lng;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Lat, Lng);
        }

        public static bool operator ==(LatLang lang1, LatLang lang2)
        {
            return EqualityComparer<LatLang>.Default.Equals(lang1, lang2);
        }

        public static bool operator !=(LatLang lang1, LatLang lang2)
        {
            return !(lang1 == lang2);
        }
    }
}