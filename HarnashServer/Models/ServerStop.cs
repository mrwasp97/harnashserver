﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class ServerStop
    {
        public Stop Stop;
        public List<TimeInfo> TimeSpans;

        public ServerStop(Stop stop, List<TimeInfo> ti)
        {
            TimeSpans = new List<TimeInfo>();
            this.Stop = stop;
            foreach(var t in ti)
            {
                TimeSpans.Add(t);
            }
        }
    }
}
