﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Models
{
    public class LineData
    {
        public string Line;
        public string From;
        public string To;
    }
}
