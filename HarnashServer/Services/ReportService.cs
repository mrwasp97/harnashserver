﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarnashServer.Models;

namespace HarnashServer.Services
{
    public class ReportService : IReportService
    {
        private List<ExistingReport> reports;

        public ReportService()
        {
            this.reports = new List<ExistingReport>();
        }

        public ExistingReport AddReport(ReportReceive report, LatLang location, IDataService dataService)
        {
            ClearExpired();
            var mr = reports.Where(r => r.Match(report, dataService));
            if(mr.Count() == 0)
            {
                ExistingReport r = new ExistingReport(report, location); //TODO: Existing number bump
                reports.Add(r);
                return r;
            }
            mr.First().Report.NumberOfReports++;
            mr.First().Refresh();
            return null;
        }

        public IEnumerable<ExistingReport> GetAll()
        {
            ClearExpired();
            return reports.AsEnumerable();
        }

        public IEnumerable<Report> GetReports(IEnumerable<LinePreference> linePreferences, IDataService dataService)
        {
            ClearExpired();
            List<Report> report = new List<Report>();
            foreach(var er in reports)
            {
                foreach (var lp in linePreferences)
                {
                    if (lp.Match(er, dataService))
                    {
                        report.Add(new Report
                        {
                            TransportNumber = er.Report.TransportNumber,
                            From = er.Report.From,
                            Lat = er.Report.Lat,
                            Lng = er.Report.Lng,
                            Delay = er.Report.Delay,
                            NumberOfReports = er.Report.NumberOfReports,
                            ReportType = er.Report.ReportType,
                            To = er.Report.To,
                            Stop = lp.BusStation,
                            Hour = lp.Hour,
                            Minute = lp.Minute
                        });
                    }
                }
            }
            return report.AsEnumerable();
        }

        private void ClearExpired()
        {
            DateTime now = DateTime.Now;
            reports.RemoveAll(r => r.ExpiresIn < now);
        }
    }
}
