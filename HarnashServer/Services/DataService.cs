﻿using HarnashServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarnashServer.Services
{
    public class DataService : IDataService
    {
        private Dictionary<string, LineInfo> lines;

        public DataService(List<string> stringLines)
        {
            lines = new Dictionary<string, LineInfo>();

            string ln = "";
            string from = "";
            string to = "";
            List<RouteInfo> routes = new List<RouteInfo>();
            List<ServerStop> stops = new List<ServerStop>();
            List<TimeInfo> timeinfos = new List<TimeInfo>();
            Stop s = null;
            foreach(string line in stringLines)
            {
                if (line.StartsWith('*'))
                {
                    string[] data = line.Substring(1).Split('-');
                    timeinfos.Add(new TimeInfo(int.Parse(data[0]), int.Parse(data[1])));
                }
                else
                {
                    if (s != null && timeinfos.Count > 0)
                    {
                        stops.Add(new ServerStop(s, timeinfos));
                        timeinfos.Clear();
                    }
                }

                if (line.StartsWith('#'))
                {
                    if(stops.Count != 0)
                    {
                        routes.Add(new RouteInfo(from, to, stops));
                        stops.Clear();
                    }
                    string[] data = line.Substring(1).Split(':');
                    if (ln != "" && ln != data[0])
                    {
                        lines[ln] = new LineInfo(routes);
                        routes.Clear();
                    }
                    ln = data[0];
                    from = data[1];
                    to = data[2];
                }

                if(line.StartsWith('$'))
                {
                    string[] data = line.Substring(1).Split(':');
                    double.TryParse(data[1], out double lat);
                    double.TryParse(data[2], out double lng);
                    s = new Stop(data[0], data[3], int.Parse(data[4]), lat, lng);
                    
                }
            }
        }

        public bool CompareTime(ReportReceive reportReceive, ExistingReport report)
        {
            Stop s1 = lines[reportReceive.Line].Routes.Where(r => r.From == reportReceive.From && r.To == reportReceive.To).First().Stops.Where(s => s.Stop.Name == reportReceive.Stop).First().Stop;
            Stop s2 = lines[report.Report.TransportNumber].Routes.Where(r => r.From == report.Report.From && r.To == report.Report.To).First().Stops.Where(s => s.Stop.Name == report.Stop).First().Stop;

            DateTime d1 = new DateTime(2000, 1, 1, reportReceive.Hour, reportReceive.Minute, 0);
            DateTime d2 = new DateTime(2000, 1, 1, report.Hour, report.Minute, 0);

            return d1.AddMinutes(-s1.Delay) == d2.AddMinutes(-s2.Delay);
        }

        public bool CompareTime(LinePreference linePreference, ExistingReport report)
        {
            Stop s1 = lines[linePreference.Line].Routes.SelectMany(r => r.Stops).Where(s => s.Stop.Name == linePreference.BusStation).First().Stop;
            Stop s2 = lines[report.Report.TransportNumber].Routes.Where(r => r.From == report.Report.From && r.To == report.Report.To).First().Stops.Where(s => s.Stop.Name == report.Stop).First().Stop;

            DateTime d1 = new DateTime(2000, 1, 1, linePreference.Hour, linePreference.Minute, 0);
            DateTime d2 = new DateTime(2000, 1, 1, report.Hour, report.Minute, 0);

            return d1.AddMinutes(-s1.Delay) == d2.AddMinutes(-s2.Delay);
        }

        public IEnumerable<(string, LineInfo)> GetAllLineInfos()
        {
            return lines.Select(kvp=>(kvp.Key, kvp.Value));
        }

        public LatLang GetLatLang(string line, string from, string to, string name)
        {
            return lines[line].Routes.Where(r => r.From == from && r.To == to).First().Stops.Where(s=>s.Stop.Name==name).Select(s=>new LatLang { Lat = s.Stop.Lat, Lng = s.Stop.Lng }).First();
        }

        public LineInfo GetLineInfo(string line)
        {
            return lines[line];
        }
    }
}
