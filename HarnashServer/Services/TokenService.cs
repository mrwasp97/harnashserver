﻿using HarnashServer.Models;
using System.Collections.Generic;
using System.Linq;

namespace HarnashServer.Services
{
    public class TokenService : ITokenService
    {
        private Dictionary<string, List<LinePreference>> tokens;

        public TokenService()
        {
            tokens = new Dictionary<string, List<LinePreference>>();
        }

        public IEnumerable<string> GetAllTokens()
        {
            return tokens.Keys.AsEnumerable();
        }

        public IEnumerable<string> GetTokensFor(ExistingReport report, IDataService dataService)
        {
            return tokens.Keys.AsEnumerable().Where(s => tokens[s].Any(p => p.Match(report, dataService)));
        }

        public bool AddToken(string token)
        {
            if(tokens.ContainsKey(token))
            {
                return false;
            }
            tokens.Add(token, new List<LinePreference>());
            return true;
        }

        public bool AddPreference(string token, LinePreference preference)
        {
            if (tokens.ContainsKey(token) == false)
            {
                return false;
            }
            tokens[token].Add(preference);
            return true;
        }

        public IEnumerable<LinePreference> GetLinePreferences(string token)
        {
            if(tokens.ContainsKey(token) == false)
            {
                return new List<LinePreference>().AsEnumerable();
            }
            return tokens[token].AsEnumerable();
        }

        public bool RemovePreference(string token, LinePreference preference)
        {
            if (tokens.ContainsKey(token) == false)
            {
                return false;
            }
            tokens[token].RemoveAll(p => p == preference);
            return true;
        }
    }
}
