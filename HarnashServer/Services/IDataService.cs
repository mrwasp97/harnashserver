﻿using HarnashServer.Models;
using System.Collections;
using System.Collections.Generic;

namespace HarnashServer.Services
{
    public interface IDataService
    {
        LineInfo GetLineInfo(string line);
        IEnumerable<(string, LineInfo)> GetAllLineInfos();
        LatLang GetLatLang(string line, string from, string to, string name);
        bool CompareTime(ReportReceive reportReceive, ExistingReport report);
        bool CompareTime(LinePreference linePreference, ExistingReport report);
    }
}