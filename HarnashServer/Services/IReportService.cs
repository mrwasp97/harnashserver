﻿using HarnashServer.Models;
using System.Collections.Generic;

namespace HarnashServer.Services
{
    public interface IReportService
    {
        ExistingReport AddReport(ReportReceive report, LatLang location, IDataService dataService);
        IEnumerable<Report> GetReports(IEnumerable<LinePreference> linePreferences, IDataService dataService);
        IEnumerable<ExistingReport> GetAll();
    }
}
