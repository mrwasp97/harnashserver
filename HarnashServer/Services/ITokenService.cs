﻿using HarnashServer.Models;
using System.Collections.Generic;

namespace HarnashServer.Services
{
    public interface ITokenService
    {
        bool AddToken(string token);
        IEnumerable<string> GetAllTokens();
        bool AddPreference(string token, LinePreference preference);
        IEnumerable<string> GetTokensFor(ExistingReport report, IDataService dataService);
        IEnumerable<LinePreference> GetLinePreferences(string token);
        bool RemovePreference(string token, LinePreference preference);
    }
}