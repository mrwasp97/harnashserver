﻿using HarnashServer.Models;
using HarnashServer.Services;
using Microsoft.AspNetCore.Mvc;

namespace HarnashServer.Controllers
{
    public class RegisterController : ControllerBase
    {
        private readonly ITokenService tokenService;

        public RegisterController(ITokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        [Route("register")]
        [HttpPut]
        public IActionResult RegisterToken([FromBody] string token)
        {
            if(string.IsNullOrEmpty(token))
            {
                return BadRequest();
            }
            if(tokenService.AddToken(token))
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        [Route("preferences/{token}")]
        [HttpPut]
        public IActionResult AddPreference([FromRoute] string token, [FromBody] LinePreference preference)
        {
            if (tokenService.AddPreference(token, preference))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [Route("preferences/{token}")]
        [HttpDelete]
        public IActionResult RemovePreference([FromRoute] string token, [FromBody] LinePreference preference)
        {
            if (tokenService.RemovePreference(token, preference))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [Route("preferences/{token}")]
        [HttpGet]
        public IActionResult GetPreferences([FromRoute] string token)
        {
            return Ok(tokenService.GetLinePreferences(token));
        }
    }
}