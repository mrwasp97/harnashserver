﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using HarnashServer.Models;
using HarnashServer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HarnashServer.Controllers
{
    public class NotificationController : ControllerBase
    {
        private readonly ITokenService tokenService;
        private readonly IReportService reportService;
        private readonly IConfiguration configuration;
        private readonly IDataService dataService;

        public NotificationController(ITokenService tokenService, IReportService reportService, IConfiguration configuration, IDataService dataService)
        {
            this.tokenService = tokenService;
            this.reportService = reportService;
            this.configuration = configuration;
            this.dataService = dataService;
        }

        [Route("reports")]
        [HttpPost]
        public async Task<IActionResult> AddReport([FromBody] ReportReceive report)
        {
            var app = FirebaseApp.DefaultInstance ?? FirebaseApp.Create(new AppOptions
            {
                Credential = GoogleCredential.FromJson(configuration.GetConnectionString("Firebase"))
            });

            if (report == null)
            {
                return BadRequest();
            }

            ExistingReport addedReport = reportService.AddReport(report, dataService.GetLatLang(report.Line, report.From, report.To, report.Stop), dataService);
            if (addedReport == null) return Ok();

            var messaging = FirebaseMessaging.GetMessaging(app);
            IReadOnlyList<string> tokens = new List<string>(tokenService.GetTokensFor(addedReport, dataService)).AsReadOnly();
            if (tokens.Count == 0) return Ok(tokens.Count);

            var message = new MulticastMessage
            {
                Tokens = tokens,
                Notification = new Notification { Title = addedReport.Report.ReportType.ToString(), Body = "Line: " + addedReport.Report.TransportNumber },
            };
            try
            {
                BatchResponse response = await messaging.SendMulticastAsync(message);
                return Ok(tokens.Count);
            }
            catch (FirebaseException e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Route("reports/{token}")]
        [HttpGet]
        public IActionResult GetReports([FromRoute] string token)
        {
            IEnumerable<LinePreference> linePreferences = tokenService.GetLinePreferences(token);
            IEnumerable<Report> reports = reportService.GetReports(linePreferences, dataService);
            return Ok(reports);
        }

        [Route("test")]
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(reportService.GetAll());
        }
    }
}