﻿using System.Collections.Generic;
using System.Linq;
using HarnashServer.Models;
using HarnashServer.Services;
using Microsoft.AspNetCore.Mvc;

namespace HarnashServer.Controllers
{
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IDataService dataService;

        public DataController(IDataService dataService)
        {
            this.dataService = dataService;
        }

        [Route("lines")]
        [HttpGet]
        public IEnumerable<LineData> GetAll()
        {
            List<LineData> ld = new List<LineData>();
            foreach((string line, LineInfo li) in dataService.GetAllLineInfos())
            {
                foreach(var r in li.Routes)
                {
                    ld.Add(new LineData { Line = line, From=r.From, To=r.To });
                }
            }
            return ld.AsEnumerable();
        }

        [Route("lines/{line}")]
        [HttpGet]
        public IEnumerable<StopData> GetLine([FromRoute] string line, [FromQuery] string from, [FromQuery] string to)
        {
            return dataService.GetLineInfo(line).Routes.Where(r=>r.From==from&&r.To==to).First().Stops.Select(s=>new StopData { Name=s.Stop.Name });
        }

        [Route("lines/{line}/{stop}")]
        [HttpGet]
        public IEnumerable<TimeInfo> GetTimeInfos([FromRoute] string line, [FromRoute] string stop)
        {
            List<TimeInfo> tis = new List<TimeInfo>();
            foreach(var r in dataService.GetLineInfo(line).Routes)
            {
                foreach(var s in r.Stops)
                {
                    if(s.Stop.Name == stop)
                    {
                        foreach(var t in s.TimeSpans)
                        {
                            tis.Add(t);
                        }
                    }
                }
            }
            return tis;
        }
    }
}