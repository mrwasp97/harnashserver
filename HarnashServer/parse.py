import re

line_regex = re.compile('.*?Linia:\s*(\S*).*')
inter_regex = re.compile('.*?r\s(\d{6}).*?\|([ \d]*?)\|.*')
dir_regex = re.compile('.*?,\s*([^,]*),.*?==>\s*([^,]*).*')
stop_regex = re.compile('(?:(?:\s+(\d{4})\s+([^,]*?))(?:,|\s\s+)).*')
sstop_regex = re.compile('\s*\d{4}(\d{2})\s+.*Y=\s*([\d\.y]*)\s+X=\s*([\d\.x]*)\s*')
hour_regex = re.compile('\s+G\s+\d+\s+(\d+):((?:(?:\s+\[da-z]+\^)|(?:\s+\[\d+\]))+)')
shours_regex = re.compile('\s+(\d{6}).*')

stops = dict()
shm = dict()
sto = list()

with open('lines.txt', 'w') as of, open('RA190518.TXT', 'r') as f:
    l = f.readline()
    ll = False
    lw = False
    zp = False
    wg = False
    rp = False
    i=0
    while l:
        if rp:
            match = shours_regex.match(l)
            if match:
                stpn = match.group(1)
                if not ln + stpn in shm:
                    wg = True
                    shm[ln + stpn] = list()
        if wg:
            match = hour_regex.match(l)
            if match:
                h = match.group(1)
                m = list()
                s = ''
                sn = False
                for c in match.group(2):
                    if str.isdigit(str(c)):
                        sn = True
                    else:
                        sn = False
                        if s != '':
                            m.append(s)
                        s = ''
                    if sn:
                        s = s + c
                if s != '':
                    m.append(s)
                for min in m:
                    shm[ln + stpn].append('*' + h + '-' + min + '\n')
                #print('m:', len(m))
        if zp:
            match = stop_regex.match(l)
            if match:
                stopname = match.group(2)
                stopnum = match.group(1)
            match = sstop_regex.match(l)
            if match:
                num = match.group(1)
                lat = match.group(2)
                lng = match.group(3)
                stops[stopnum + num] = (stopname + ' ' + num, lat, lng)
        if ll:
            match = line_regex.match(l)
            if match:
                ln = match.group(1)
            match = dir_regex.match(l)
            if match:
                for p in sto:
                    of.write(p[1])
                    #print('smh:', len(shm[p[0]]))
                    for t in shm[p[0]]:
                        of.write(t)
                sto = list()
                of.write('#' + ln + ':' + match.group(1) + ':' + match.group(2) +'\n')
        if lw:
            match = inter_regex.match(l)
            if match:
                s = match.group(1)
                h = match.group(2)
                ss = stops[s]
                sto.append((ln+s, '$' + ss[0]+':'+ss[1]+':'+ss[2]+':'+s + ':' + h + '\n'))
        if '*LL' in l or '#WK' in l:
            ll = True
        if '*LW' in l:
            lw = True
        if '*RP' in l:
            rp = True
        if '*ZP' in l:
            zp = True
        if '#ZP' in l:
            zp = False
        if '#LW' in l:
            lw = False
        if '#WG' in l:
            wg = False
            
        l = f.readline()
        i = i+1
